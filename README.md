# README #

Code implemented for the approval of the Standford "Machine Learning" Course


### What is this repository for? ###

* Octave/Matlab code for each of the 8 programming excersises
* 1.0
* [Machine Learning](https://www.coursera.org/learn/machine-learning)

### How do I get set up? ###

* Set up Octave

### Who do I talk to? ###

* Mauricio van der Maesen de Sombreff
* mauriciovander@gmail.com
