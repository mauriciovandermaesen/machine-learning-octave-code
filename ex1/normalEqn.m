function [theta] = normalEqn(X, y)
%NORMALEQN Computes the closed-form solution to linear regression 
%   NORMALEQN(X,y) computes the closed-form solution to linear 
%   regression using the normal equations.

theta = zeros(size(X, 2), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the code to compute the closed form solution
%               to linear regression and put the result in theta.
%

% ---------------------- Sample Solution ----------------------

% REGULARIZATION PARAMETER
lambda=size(X, 2);

L = eye(size(X, 2),size(X, 2));
L(1,1)=0;

theta = pinv(X'* X + lambda * L )*X'*y;

% -------------------------------------------------------------


% ============================================================

end
