function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));


X = [ones(m,1) X];

for t=1:m
  Y = 1:num_labels==y(t);
 
  a1 = X(t,:);
  z2 = Theta1 * a1';
  a2 = [1; sigmoid(z2)];
  z3 = Theta2 * a2;
  a3 = sigmoid(z3);
  
  J -= Y * log(a3) + (1 - Y) * log(1 - a3);
 
  % backprop
  delta3 = a3 - Y';
  delta2 = (Theta2' * delta3) .* [1; sigmoidGradient(z2)];
  delta2 = delta2(2:end);
 
  Theta1_grad += delta2 * a1;
  Theta2_grad += delta3 * a2';
end
 
Theta1_grad = (1 / m) * Theta1_grad + (lambda / m) * [zeros(size(Theta1, 1), 1) Theta1(:,2:end)];
Theta2_grad = (1 / m) * Theta2_grad + (lambda / m) * [zeros(size(Theta2, 1), 1) Theta2(:,2:end)];
 
% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];

Theta1(:,1) = 0;
Theta2(:,1) = 0;
tt = [Theta1(:) ; Theta2(:)];
J += tt'*tt*lambda/2;
 

J /= m;

end
